//====================== OneWire ====================================
#include <OneWire.h>
OneWire ds2  (2);
String res = "";
byte type_s;
byte data[12];
byte addr_ds2[8];
bool temp_sensor_2_found = false;
float temp1 = 999;
//===================================================================

#include <SPI.h>        
#include "DMD.h"       
#include "TimerOne.h"   
#include "SystemFont5x7rus.h"

#define CLR 0x0C
#define US 0x1F
#define ESC 0x1B
#define LF 0x0A

static const char ID[] = "P10_DISPLAY_V01\r\n";    // Device ID

#define DISPLAYS_ACROSS 1
#define DISPLAYS_DOWN 1
DMD dmd(DISPLAYS_ACROSS, DISPLAYS_DOWN); // Конфигурация экрана, панелей может быть не одна.
#define max_char1 6
#define max_char2 176
unsigned char message1[max_char1] = {0x20,0x30,0x30,0x30,0x30,0x00};    // stores you message
unsigned char message2[max_char2];    // stores you message

unsigned char r_char;               
byte index1 = 4;            
byte index2 = 176;            
int i;
long scrollSpeed = 25;            

char test[] = { 0x84,0x80,0x20,0x87,0x84,0x90,0x80,0x82,0x91,0x92,0x82,0x93,0x85,0x92,0x20,0x92,
                0x8E,0x20,0x81,0x8B,0x80,0x83,0x8E,0x84,0x80,0x90,0x9F,0x20,0x97,0x85,0x8C,0x93,
                0x20,0x8C,0x9B,0x20,0x8D,0x85,0x91,0x8C,0x8E,0x92,0x90,0x9F,0x20,0x8D,0x88,0x20,
                0x8D,0x80,0x20,0x97,0x92,0x8E,0x20,0x88,0x20,0x82,0x8E,0x8F,0x90,0x85,0x8A,0x88,
                0x20,0x82,0x91,0x85,0x8C,0x93,0x21,0x00};

//=============================================================================
bool get_sensor(byte addr[8], OneWire ds) {

  // Ищем адрес датчика
  if  (! ds.search  (addr)) {
    ds.reset_search  ();
    delay  (250);
    return false;
  }
  
  // Проверяем не было ли помех при передаче
  if (OneWire::crc8(addr, 7) != addr[7])
  {
    Serial.println("get_sensor:CRC is not valid!");
    return false;
  }

  // Определяем серию датчика
  switch (addr[0])
  {
    case 0x10:
      //Chip = DS18S20
      type_s = 1;
      break;
    case 0x28:
      //Chip = DS18B20
      type_s = 0;
      break;
    case 0x22:
      //Chip = DS1822
      type_s = 0;
      break;
    default:
      Serial.println("get_sensor:Device is not a DS18x20 family device.");
      return false;
  }
  return true;
}

//=============================================================================

double get_temp(byte addr[8], OneWire ds) {

  double celsius;

  ds.reset  ();
  ds.select  (addr);   // Выбираем адрес
  ds.write  (0x44, 1); // Производим замер, в режиме паразитного питания
  delay  (750); 
  ds.reset  ();
  ds.select  (addr);
  ds.write  (0xBE); // Считываем оперативную память датчика

  for  (int i = 0; i < 9; i++) { // Заполняем массив считанными данными
    data[i] = ds.read  ();
  }

  // Данные о температуре содержатся в первых двух байтах, переведем их в одно значение и преобразуем в шестнадцатиразрядное число
  int raw =  (data[1] << 8) | data[0];
  if  (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if  (data[7] == 0x10) {
      raw =  (raw & 0xFFF0) + 12 - data[6];
    }
  }
  else
  {
    byte cfg =  (data[4] & 0x60);
    if  (cfg == 0x00) raw = raw << 3; 
    else if  (cfg == 0x20) raw = raw << 2; 
    else if  (cfg == 0x40) raw = raw << 1; 
  }
    
  celsius =  (double)raw / 16.0;
  return celsius;
};

//=============================================================================
void massage2BufClear ()
{
  for(i=0; i<max_char2; i++)
		{
            message2[i] = '\0';
        }        
        index2=0;
}
//--------------------------------------------------------------
void massage1BufClear ()
{

        message1[0] = 0x20;
        message1[1] = 0x30;
        message1[2] = 0x30;
        message1[3] = 0x30;
        message1[4] = 0x30;
        message1[5] = 0x00;    
        index1=0;        
}

//--------------------------------------------------------------
void massage1Normalization ()
{
  
  char buf[5];

  buf[0] = 0x20;
  buf[1] = 0x30;
  buf[2] = 0x30;
  buf[3] = 0x30;
  buf[4] = 0x30;

  int char_count = index1;
        
   for(i = char_count - 1; i >= 0; i--)
   {
      buf[i] = message1[i];      
   }
      
   massage1BufClear();

   for(i = char_count - 1; i >= 0; i--)
   {
      message1[i+5-char_count] = buf[i];      
   }        
}
//------------------------------------------------------------------
void ScanDMD()
{ 
  dmd.scanDisplayBySPI();
}
//------------------------------------------------------------------
void setup(void)
{
   Timer1.initialize( 500 );          
   Timer1.attachInterrupt( ScanDMD );   
   dmd.clearScreen( true );   
   Serial.begin(9600);
   strcpy(message2,test);
   dmd.brightness = 70;
   temp_sensor_2_found = get_sensor(addr_ds2, ds2);
   dmd.selectFont(SystemFont5x7);
}
//------------------------------------------------------------------
void loop(void)
{    
   dmd.drawMarquee(message2 ,index2,(32*DISPLAYS_ACROSS)-1 ,8);

    if (temp_sensor_2_found)                  // !!!!!!!
    {                                         // !!!!!!!
     res = String(get_temp(addr_ds2, ds2));   // !!!!!!!
     res.toCharArray(message1, 5);            // !!!!!!!
     message1[4] = 0xF8;                      // !!!!!!!
    }                                         // !!!!!!!

   long start=millis();
   long timer=start;
   boolean ret=false; 
   while(!ret)
   {
     if ((timer + scrollSpeed) < millis()) {
        dmd.drawFilledBox( 0,0,31,7, GRAPHICS_INVERSE);
        ret=dmd.stepMarquee(-1,0);
        timer=millis();
        dmd.drawString(  1,  0, message1, 5, GRAPHICS_NORMAL );      
        if(Serial.available())
          break;        
     }
   }
}
//-----------------------------------------------------------------------------------
void serialEvent() {
      
      delay(25); // Wait all data
      r_char = Serial.read();

      if(r_char < 0x20)
      {
          switch (r_char) 
          {
              case ESC:
                r_char = Serial.read();
                if(r_char=='@')
                {
                  Serial.write(ID);
                }
                break;             
              
              case CLR:
                massage1BufClear();
                massage2BufClear();              
                break;
              
              case US:
                r_char = Serial.read();                
                if(r_char=='X')
                {
                  dmd.brightness = Serial.read();   
                }       
                
                if(r_char=='B'){
                  massage2BufClear();

                  while(Serial.available() > 0)
                  {
                    r_char = Serial.read();
                    if(index2 < (max_char2-1)) 
                      {         
                        if(r_char > 0x1F)
                        {
                            message2[index2] = r_char;     
                            index2++;                     
                        }
                      }               
                  }
                }

                  if(r_char==LF){
                          massage1BufClear();
                          
                          while(Serial.available() > 0)
                                  {
                                    r_char = Serial.read();         
                                    if(index1 < (max_char1-1)) 
                                      {                                 
                                        if((r_char > 0x29) and (r_char < 0x3A))
                                        {
                                            message1[index1] = r_char;     
                                            index1++;                     
                                        }
                                      }
                                                 
                                  }
                          massage1Normalization();   
                }               
                dmd.drawFilledBox( 0,8,31,15, GRAPHICS_INVERSE);
                break;
              
              default:
                break;
            }       
          }
        
   while (Serial.available()) Serial.read();
   dmd.writePixel(0,0,GRAPHICS_NORMAL,1);
}


